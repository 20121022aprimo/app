﻿using System;
using System.Collections.Generic;

namespace app.utility.containers
{
  public class ItemFactories : IFindFactoriesThatCreateItems
  {
    IDictionary<Type, ICreateOneDependency> factories;

    public ItemFactories(IDictionary<Type, ICreateOneDependency> Factories)
    {
      factories = Factories;
    }

    public ICreateOneDependency get_the_factory_that_can_create(Type type)
    {
      return factories[type];
    }
  }
}