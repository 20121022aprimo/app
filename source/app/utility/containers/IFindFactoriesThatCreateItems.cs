﻿using System;

namespace app.utility.containers
{
  public interface IFindFactoriesThatCreateItems
  {
    ICreateOneDependency get_the_factory_that_can_create(Type type);
  }
}