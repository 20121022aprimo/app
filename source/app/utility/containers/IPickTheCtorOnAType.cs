﻿using System;
using System.Reflection;

namespace app.utility.containers
{
  public interface IPickTheCtorOnAType
  {
    ConstructorInfo get_the_ctor_on(Type type);
  }
}