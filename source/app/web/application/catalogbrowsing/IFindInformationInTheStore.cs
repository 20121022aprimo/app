﻿using System.Collections.Generic;

namespace app.web.application.catalogbrowsing
{
  public interface IFindInformationInTheStore
  {
    IEnumerable<DepartmentItem> get_the_main_departments_in_the_store();
	  IEnumerable<DepartmentItem> get_departments_using(ViewDepartmentsInDepartmentRequest request);
    IEnumerable<ProductItem> get_the_products_using(ViewProductsInADepartmentRequest specfic_request);

  }
}