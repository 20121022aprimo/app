﻿using System.Collections.Generic;
using System.Linq;

namespace app.web.application.catalogbrowsing.stubs
{
  public class StubStoreCatalog : IFindInformationInTheStore
  {
    public IEnumerable<DepartmentItem> get_the_main_departments_in_the_store()
    {
      return Enumerable.Range(1, 100).Select(x => new DepartmentItem{name = x.ToString("Department 0")});
    }


    public IEnumerable<DepartmentItem> get_departments_using(ViewDepartmentsInDepartmentRequest request)
    {
      return Enumerable.Range(1, 100).Select(x => new DepartmentItem{name = x.ToString("Sub Department 0")});
    }

    public IEnumerable<ProductItem> get_the_products_using(ViewProductsInADepartmentRequest specfic_request)
    {
      return Enumerable.Range(1, 100).Select(x => new ProductItem{name = x.ToString("Product 0")});
    }
  }
}