﻿using System.Collections;
using System.Collections.Generic;
using app.web.application.catalogbrowsing;
using app.web.application.catalogbrowsing.stubs;

namespace app.web.core.stubs
{
  public class StubSetOfCommands : IEnumerable<IProcessOneRequest>
  {
    IEnumerator IEnumerable.GetEnumerator()
    {
      return GetEnumerator();
    }

    public class GetTheMainDepartments : IFetchAReport<IEnumerable<DepartmentItem>>
    {
      public IEnumerable<DepartmentItem> fetch_using(IContainRequestInformation request)
      {
        return new StubStoreCatalog().get_the_main_departments_in_the_store();
      }
    }

    public IEnumerator<IProcessOneRequest> GetEnumerator()
    {
      yield break ;
    }
  }
}