﻿using System;

namespace app.web.core.aspet
{
  public delegate object CreatePage_Behaviour(string path, Type page_type);
}