﻿using System;
using System.Collections.Generic;
using app.web.application.catalogbrowsing;

namespace app.web.core.aspet.stubs
{
  public class StubPathRegistry : IFindPathsToAspxs

  {
    public string get_the_path_to_the_logical_view_for<ReportModel>()
    {
      var paths = new Dictionary<Type, string>
      {
        {typeof(IEnumerable<DepartmentItem>), create_view_for("DepartmentBrowser")},
        {typeof(IEnumerable<ProductItem>), create_view_for("ProductBrowser")}
      };
      return paths[typeof(ReportModel)];
    }

    string create_view_for(string page)
    {
      return string.Format("~/views/{0}.aspx", page);
    }
  }
}