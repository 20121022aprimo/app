﻿namespace app.web.core.aspet
{
  public interface IFindPathsToAspxs
  {
    string get_the_path_to_the_logical_view_for<ReportModel>();
  }
}