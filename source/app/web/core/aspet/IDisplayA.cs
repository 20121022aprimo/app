﻿using System.Web;
using System.Web.UI;

namespace app.web.core.aspet
{
  public interface IDisplayA<ReportModel> : IHttpHandler
  {
    ReportModel report { get; set; }
  }

  public class LogicalViewFor<ReportModel> : Page, IDisplayA<ReportModel>
  {
    public ReportModel report { get; set; }
  }
}