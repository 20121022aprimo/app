﻿namespace app.web.core
{
  public delegate bool RequestMatch_Behaviour(IContainRequestInformation request);
}