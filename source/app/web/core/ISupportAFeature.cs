﻿namespace app.web.core
{
  public interface ISupportAFeature
  {
    void run(IContainRequestInformation request);
  }
}