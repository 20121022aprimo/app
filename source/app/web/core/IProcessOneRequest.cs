﻿namespace app.web.core
{
  public interface IProcessOneRequest : ISupportAFeature
  {
    bool can_process(IContainRequestInformation request);
  }

  public class RequestCommand : IProcessOneRequest
  {
    RequestMatch_Behaviour request_specification;
    ISupportAFeature feature;

    public RequestCommand(RequestMatch_Behaviour request_specification, ISupportAFeature feature)
    {
      this.request_specification = request_specification;
      this.feature = feature;
    }

    public void run(IContainRequestInformation request)
    {
      feature.run(request);
    }

    public bool can_process(IContainRequestInformation request)
    {
      return request_specification(request);
    }
  }
}