﻿namespace app.web.core
{
  public interface IContainRequestInformation
  {
    InputModel map<InputModel>();
  }
}