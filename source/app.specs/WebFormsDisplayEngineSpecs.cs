﻿using System.Web;
using Machine.Specifications;
using app.specs.utility;
using app.web.core;
using app.web.core.aspet;
using developwithpassion.specifications.rhinomocks;
using developwithpassion.specifications.extensions;

namespace app.specs
{
  [Subject(typeof(WebFormsDisplayEngine))]
  public class WebFormsDisplayEngineSpecs
  {
    public abstract class concern : Observes<IDisplayInformation,
                                      WebFormsDisplayEngine>
    {
    }

    public class when_displaying_a_report : concern
    {
      Establish c = () =>
      {
        the_report = new AnItemToDisplay();
        view_factory = depends.on<ICreateViewsForReports>();
        the_current_context = ObjectFactory.web.create_http_context();
        depends.on<GetTheCurrentRequest_Behaviour>(() => the_current_context);
        the_view = fake.an<IHttpHandler>();

        view_factory.setup(x => x.create_view_that_can_display(the_report)).Return(the_view);
      };

      Because b = () =>
        sut.display(the_report);

      It should_tell_the_view_to_render = () =>
        the_view.received(x => x.ProcessRequest(the_current_context));
        
        
      static AnItemToDisplay the_report;
      static ICreateViewsForReports view_factory;
      static IHttpHandler the_view;
      static HttpContext the_current_context;
    }

    public class AnItemToDisplay
    {
    }
  }
}