﻿using Machine.Specifications;
using app.web.core;
using developwithpassion.specifications.extensions;
using developwithpassion.specifications.rhinomocks;

namespace app.specs
{
  [Subject(typeof(RequestCommand))]
  public class ProcessOneRequestSpecs
  {
    public abstract class concern : Observes<IProcessOneRequest,
                                      RequestCommand>
    {
    }

    public class when_processing_a_request : concern
    {
      Establish c = () =>
      {
        request = fake.an<IContainRequestInformation>();
        feature = depends.on<ISupportAFeature>();
      };

      Because b = () =>
        sut.run(request);

      It should_trigger_the_application_specific_functionality = () =>
        feature.received(x => x.run(request));

      static ISupportAFeature feature;
      static IContainRequestInformation request;
    }

    public class when_determining_if_it_can_process_a_request : concern
    {
      Establish c = () =>
      {
        request = fake.an<IContainRequestInformation>();
        depends.on<RequestMatch_Behaviour>(x =>
        {
          x.ShouldEqual(request);
          return true;
        });
      };

      Because b = () =>
        result = sut.can_process(request);

      It should_make_its_determination_by_using_its_request_specification = () =>
        result.ShouldBeTrue();

      static IContainRequestInformation request;
      static bool result;
    }
  }
}