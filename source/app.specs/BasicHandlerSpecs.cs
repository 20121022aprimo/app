﻿ using System.Web;
 using Machine.Specifications;
 using app.specs.utility;
 using app.web.core;
 using app.web.core.aspet;
 using developwithpassion.specifications.rhinomocks;
 using developwithpassion.specifications.extensions;

namespace app.specs
{  
  [Subject(typeof(BasicHandler))]  
  public class BasicHandlerSpecs
  {
    public abstract class concern : Observes<IHttpHandler,
                                      BasicHandler>
    {
        
    }

   
    public class when_processing_an_incoming_http_context : concern
    {
      Establish c = () =>
      {
        front_controller = depends.on<IProcessRequests>();
        a_new_request = fake.an<IContainRequestInformation>();
        context = ObjectFactory.web.create_http_context();
        request_factory = depends.on<ICreateRequests>();

        request_factory.setup(x => x.create_request_from(context)).Return(a_new_request);
      };

      Because b = () =>
        sut.ProcessRequest(context);


      It should_delegate_the_processing_of_a_new_request_to_our_front_controller = () =>
        front_controller.received(x => x.process(a_new_request));

      static IProcessRequests front_controller;
      static IContainRequestInformation a_new_request;
      static HttpContext context;
      static ICreateRequests request_factory;
    }
  }
}
