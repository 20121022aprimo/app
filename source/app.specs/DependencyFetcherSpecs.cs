﻿using System;
using Machine.Specifications;
using app.utility.containers;
using developwithpassion.specifications.extensions;
using developwithpassion.specifications.rhinomocks;

namespace app.specs
{
  [Subject(typeof(DependencyFetcher))]
  public class DependencyFetcherSpecs
  {
    public abstract class concern : Observes<DependencyFetcher>
    {
    }

    public interface ITestInterface
    {
    }

    public class when_fetching_an_item : concern
    {
      static ITestInterface result;

      Establish c = () =>
      {
        the_item = fake.an<ITestInterface>();
        factories = depends.on<IFindFactoriesThatCreateItems>();
        the_item_factory = fake.an<ICreateOneDependency>();

        factories.setup(x => x.get_the_factory_that_can_create(typeof(ITestInterface)))
          .Return(the_item_factory);

        the_item_factory.setup(x => x.create()).Return(the_item);
      };

      Because b = () =>
        result = sut.an<ITestInterface>();

      It should_return_the_item_created_by_the_factory_that_can_create_the_specific_dependency = () =>
        result.ShouldEqual(the_item);

      static ITestInterface the_item;
      static IFindFactoriesThatCreateItems factories;
      static ICreateOneDependency the_item_factory;
    }

    public class when_fetching_an_item_at_runtime : concern
    {
      Establish c = () =>
      {
        the_item = fake.an<ITestInterface>();
        factories = depends.on<IFindFactoriesThatCreateItems>();
        the_item_factory = fake.an<ICreateOneDependency>();

        factories.setup(x => x.get_the_factory_that_can_create(typeof(ITestInterface)))
          .Return(the_item_factory);

        the_item_factory.setup(x => x.create()).Return(the_item);
      };

      Because b = () =>
        result = sut.an(typeof(ITestInterface));

      It should_return_the_item_created_by_the_factory_that_can_create_the_specific_dependency = () =>
        result.ShouldEqual(the_item);

      static ITestInterface the_item;
      static IFindFactoriesThatCreateItems factories;
      static ICreateOneDependency the_item_factory;
      static object result;
    }

    public class when_fetching_an_item_and_the_factory_for_the_item_throws_an_exception_on_creation : concern
    {
      static ITestInterface result;

      Establish c = () =>
      {
        factories = depends.on<IFindFactoriesThatCreateItems>();
        the_item_factory = fake.an<ICreateOneDependency>();
        the_new_exception = new Exception();
        inner_exception = new Exception();

        depends.on<ItemCreationFailure_Behaviour>((type, inner) =>
        {
          type.ShouldEqual(typeof(ITestInterface));
          inner.ShouldEqual(inner_exception);
          return the_new_exception;
        });

        factories.setup(x => x.get_the_factory_that_can_create(typeof(ITestInterface)))
          .Return(the_item_factory);

        the_item_factory.setup(x => x.create()).Throw(inner_exception);
      };

      Because b = () =>
        spec.catch_exception(() => sut.an<ITestInterface>());

      It should_throw_a_new_exception_created_by_the_custom_exception_factory = () =>
        spec.exception_thrown.ShouldEqual(the_new_exception);

      static IFindFactoriesThatCreateItems factories;
      static ICreateOneDependency the_item_factory;
      static Exception the_new_exception;
      static Exception inner_exception;
    }
  }
}